# Inter (NerdFont Patch)

Inter is a typeface carefully crafted & designed for computer screens.
Inter features a tall x-height to aid in readability of mixed-case and lower-case text.
Inter is a [variable font](https://rsms.me/inter/#variable) with
several [OpenType features](https://rsms.me/inter/#features), like contextual alternates that adjusts punctuation depending on the shape of surrounding glyphs, slashed zero for when you need to disambiguate "0" from "o", tabular numbers, etc.

This patch contains all of the icons provided by [NerdFonts](https://www.nerdfonts.com/).
The following flow diagram shows the current glyph sets included:
![nerd-fonts-diagram](https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/images/sankey-glyphs-combined-diagram.svg)

<br>

[![Sample](https://raw.githubusercontent.com/rsms/inter/master/misc/readme/intro.png)](https://rsms.me/inter/samples/)


### Derivative versions

- [Open Runde](https://github.com/lauridskern/open-runde) is a rounded variant of Inter
- [Interalia](https://github.com/Shavian-info/interalia) extends Inter with Shavian characters


## Notable projects using Inter

- [Element software suite](https://element.io/)
- [ElementaryOS](https://elementary.io/)
- [Figma](https://figma.com/)
- [GitLab](https://gitlab.com)
- [Minimalissimo magazine](https://minimalissimo.com/)
- [Mozilla brand](https://mozilla.design/firefox/typography/)
- [NASA](https://www.nasa.gov/specials/artemis-ii/)
- [Pixar Presto](https://en.wikipedia.org/wiki/Presto_(animation_software))
- [Unity](https://unity.com/)
- [Zurich Airport](https://flughafen-zuerich.ch/)


> **Have you made something nice with Inter?**<br>
> [Please share in Show & Tell! →](https://github.com/rsms/inter/discussions/categories/show-and-tell)


## Supporters & contributors

A wholehearted **Thank You** to everyone who supports the Inter project!

Special thanks to
[@thundernixon](https://github.com/thundernixon) and
[@KatjaSchimmel](https://github.com/KatjaSchimmel)
who have put in significant effort into making Inter what it is through
their contributions ♡

See [graphs/contributors](https://github.com/rsms/inter/graphs/contributors)
for a complete list of all contributors.


Inter is licensed under the [SIL Open Font License](LICENSE.txt)


## Design

_This section discusses some of the design choices made for Inter._

Inter can be classified as a geometric neo-grotesque, similar in style to Roboto, Apple San Francisco, Akkurat, Asap, Lucida Grande and more. Some trade-offs were made in order to make this typeface work really well at small sizes:

- Early versions of Inter was not suitable for very large sizes because of some small-scale glyph optimizations (like "pits" and "traps") that help rasterization at small sizes but stand out and interfere at large sizes. However today Inter works well at large sizes and a [Display subfamily](https://github.com/rsms/inter/releases/tag/display-beta-1) is in the works for really large "display" sizes.

- Rasterized at sizes below 12px, some stems—like the horizontal center of "E", "F", or vertical center of "m"—are drawn with two semi-opaque pixels instead of one solid. This is because we "prioritize" (optimize for) higher-density rasterizations. If we move these stems to an off-center position—so that they can be drawn sharply at e.g. 11px—text will be less legible at higher resolutions.

Inter is a [variable font](https://rsms.me/inter/#variable) and is in addition also distributed as a set of traditional distinct font files in the following styles:

| Roman (upright) name | Italic name          | Weight
| -------------------- | -------------------- | ------------
| Thin                 | Thin Italic          | 100
| Extra Light          | Extra Light Italic   | 200
| Light                | Light Italic         | 300
| Regular              | Italic               | 400
| Medium               | Medium Italic        | 500
| Semi Bold            | Semi Bold Italic     | 600
| Bold                 | Bold Italic          | 700
| Extra Bold           | Extra Bold Italic    | 800
| Black                | Black Italic         | 900

