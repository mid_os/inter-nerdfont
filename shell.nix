{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  packages = builtins.attrValues {
    inherit (pkgs)
      nerd-font-patcher
      fontforge
      python3
      font-manager;
  };
}

